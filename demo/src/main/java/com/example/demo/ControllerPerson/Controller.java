package com.example.demo.ControllerPerson;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Address;
import com.example.demo.Model.Animal;
import com.example.demo.Model.Professor;
import com.example.demo.Model.Student;
import com.example.demo.Model.Subject;
import com.example.demo.Model.Worker;

@RestController
public class Controller {
    // chuyển tất cả arraylist về 1 kiểu là object , để dễ so sánh 
    @GetMapping("person")
    public ArrayList<Object> getPerson(@RequestParam (value = "type",defaultValue = "error") String type) {

        ArrayList<Object> ArrayListPerson= new ArrayList<Object>();
        
        // cách 1 làm bằng arraylist
        Student student1 = new Student(2, new ArrayList<Subject>(){
            {
                add(new Subject("Tiếng Anh chuyên ngành" , 3 , new Professor(33 , "male" , "Giang", new Address("Lý Thường Kiệt" , "TP.HCM" , "Việt Nam",50000) ,3000)));
            }
        });

        Student student2 = new Student(23 , "male" , "Linh" , new Address("Tôn Đản" , "TP.HCM" ,"Việt Nam" ,50000) , 5 , new ArrayList<Subject>(){
            {
                add(new Subject("Hóa học Đại cương" , 3 , new Professor(50 , "male" , "Hoàng Long", new Address("Tôn Thất Thuyết" , "TP.HCM" , "Việt Nam",50000) ,3000)));
            }
        }) ;

        
        Student student3 = new Student(25 ,"female" , "Huong", new Address() , 10 , new ArrayList<Subject>(){
            {
                add(new Subject("Hóa học Hữu cơ" , 4 , new Professor(45 , "male" , "Anh Quân", new Address("Vũ Tông Phan" , "TP.HCM" , "Việt Nam",50000) ,5000)));
            }
        }, new ArrayList<Animal>());
        //public Student(int age, String gender, String name, Address address, int student, ArrayList<Subject> listSubject , ArrayList<Animal>listPet )
        // cách 2 làm bằng array 
        Student studentDiana = new Student(20, "female", "Diana", new Address(), 2, new ArrayList<Subject>(Arrays.asList(
            new Subject("Tiếng Anh cơ sở ", 7, new Professor(50, "male", "Hải Anh", new Address() , 4000)),
            new Subject("Toán Cao cấp", 4, new Professor(55, "female", "Hồng Hoa", new Address() , 5000))
        )));

        ArrayList<Object> ArrayListStudent = new ArrayList<Object>();
        ArrayListStudent.add(studentDiana);
        ArrayListStudent.add(student1);
        ArrayListStudent.add(student2);
        ArrayListStudent.add(student3);

        ArrayList<Object> ArrayListProfessor = new ArrayList<Object>();

        Professor professor = new Professor(50 , "male" , "Tấn Đạt" , new Address() , 200000101 , 
        new ArrayList<Animal>()); 

        ArrayListProfessor.add(professor);

        ArrayList<Object> ArrayListWorker = new ArrayList<Object>();

        Worker worker = new Worker(32 , "male" , "Văn Trung" , new Address("Hoàng Sa" , "TP.HCM" , "Việt Nam" , 50000) , 2000000 , 
        new ArrayList<Animal>());

        ArrayListWorker.add(worker);
        ArrayListWorker.add(worker);

        ArrayListPerson.add(student1);
        ArrayListPerson.add(student2);
        ArrayListPerson.add(studentDiana);
        ArrayListPerson.add(student3);
        ArrayListPerson.add(worker);
        ArrayListPerson.add(worker);
        ArrayListPerson.add(professor);
 
       
       

    // cách 2 
    // ArrayList<Object> Prepare = null ;
    //     if(type.equals("1")){
    //         Prepare = ArrayListStudent;
    //     }
    //     else if(type.equals("2")){
    //         Prepare = ArrayListProfessor;
    //     }
    //     else if(type.equals("3")){
    //         Prepare = ArrayListWorker;
    //     }
    //     else{
    //         Prepare = ArrayListPerson;
    //     }
    //    return Prepare;

    // cách 1
        switch(type){
            case "1" :
            return  ArrayListStudent; 
            case "2" :
            return  ArrayListProfessor ;
            case "3" :
            return  ArrayListWorker ;
            default :
            return  ArrayListPerson ;
        }
    }
   
}





