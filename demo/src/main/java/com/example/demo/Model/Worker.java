package com.example.demo.Model;

import java.util.ArrayList;

public class Worker extends Person implements ILive, IAnimal{

    private int salary ;

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Worker() {
        super();
    }

    public Worker(int salary) {
        this.salary = salary;
    }
    public Worker(int age, String gender, String name, Address address, int salary) {
        super(age, gender, name, address);
        this.salary = salary;
    }
    
    public Worker(int age, String gender, String name, Address address, int salary ,ArrayList<Animal> listPet) {
        super(age, gender, name, address ,listPet);
        this.salary = salary;
    }
    
    @Override
    public void eat() {
      System.out.println("Worker eating...");
        
    }
    public void Working(){
        System.out.println("Worker Working");
    }

    @Override
    public void animalSound() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void animalSleep() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void gotoShop() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void play() {
        // TODO Auto-generated method stub
        
    }

}
